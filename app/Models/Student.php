<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'nomor_induk', 'nama', 'kompetensi', 'tahun_pelajaran', 'kelas', 'semester'
    ];

    public function character()
    {
        return $this->hasMany(Character::class, 'siswa_id');
    }
}
