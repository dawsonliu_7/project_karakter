@extends('layouts.admin.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Form Validation</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Forms</a>
                  </li>
                  <li class="breadcrumb-item active">Form Validation
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
            <div class="dropdown">
              <a class="btn-icon btn btn-danger btn-round btn-sm dropdown-toggle" href="{{ route('siswa.index') }}">Back</a>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Validation -->
<section class="bs-validation">
<div class="row">
  <!-- Bootstrap Validation -->
  <div class="col-md-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Bootstrap Validation</h4>
      </div>
      <div class="card-body">
        <form class="needs-validation" method="POST" action="{{ route('siswa.update', $student->id) }}">
            @csrf
            @method('PUT')
          <div class="form-group">
            <label class="form-label" for="basic-addon-name">Nomor Induk (NIS/NISN)</label>

            <input
              type="text"
              id="basic-addon-name"
              class="form-control"
              value="{{ $student->nomor_induk }}"
              name="nomor_induk"
              aria-describedby="basic-addon-name"
              required
            />
          </div>
          <div class="form-group">
            <label class="form-label" for="basic-default-email1">Nama Lengkap</label>
            <input
              type="text"
              id="basic-default-email1"
              class="form-control"
              name="nama"
              value="{{ $student->nama }}"
              required
            />
          </div>
          <div class="form-group">
            <label class="form-label" for="basic-default-password1">Kompetensi</label>
            <select class="form-control" name="kompetensi" id="select-country1" required>
                <option value="" disabled selected>Pilih Kompetensi</option>
                <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                <option value="Multimedia">Multimedia</option>
                <option value="Bisnis Daring dan Pemasaran">Bisnis Daring dan Pemasaran</option>
                <option value="Teknik Komputer Jaringan">Teknik Komputer Jaringan</option>
                <option value="Tataboga">Tataboga</option>
                <option value="Otomatisasi Tata Kelola Perkantoran">Otomatisasi Tata Kelola Perkantoran</option>
                <option value="Perhotelan">Perhotelan</option>
              </select>
          </div>
          <div class="form-group">
            <label for="select-country1">Tahun Pelajaran (2018 - 2021)</label>
            <input
              type="text"
              id="basic-default-password1"
              class="form-control"
              name="tahun_pelajaran"
              value="{{ $student->tahun_pelajaran }}"
              required
            />
          </div>
          <div class="form-group">
            <label for="dob-bootstrap-val">Rombel (RPL XII-2)</label>
            <input
              type="text"
              id="dob-bootstrap-val"
              class="form-control"
              name="kelas"
              value="{{ $student->kelas }}"
              required
            />
          </div>
          <div class="form-group">
            <label for="customFile1">Semester</label>
            <div class="custom-file">
              <select class="form-control" name="semester" id="select-country1" required>
                <option value="" disabled selected>Pilih Semester</option>
                <option value="Semester 1">Semester 1</option>
                <option value="Semester 2">Semester 2</option>
                <option value="Semester 3">Semester 3</option>
                <option value="Semester 4">Semester 4</option>
              </select>
            </div>
          </div>
          <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
            <div class="alert-body">
              <i data-feather="info" class="mr-50 align-middle"></i>
              <span><strong>Note:</strong> Harap mengisi format sesuai contoh yang ada di dalam kurung!</span>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /Bootstrap Validation -->
</div>
</section>
<!-- /Validation -->

      </div>
    </div>
  </div>
@endsection
